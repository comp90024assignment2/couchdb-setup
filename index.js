const cradle = require('cradle');
const bluebird = require('bluebird');
const con = new cradle.Connection('http://localhost', 5984, {
      auth: { username: 'admin', password: 'Vai9vah6' }
  });

// Promisify everything
bluebird.promisifyAll(cradle);

const databases = [
    // System databases
    '_users',
    '_replicator',
    '_global_changes',

    // Our database
    'comp90024',
];

// Design documents
// Refer to http://guide.couchdb.org/editions/1/en/design.html#modeling and http://docs.couchdb.org/en/2.1.1/ddocs/views/index.html
const design_docs = [
    require('./design_docs/users'),
    require('./design_docs/matches'),
    require('./design_docs/teams'),
    require('./design_docs/tweets')
];

async function setup() {
    // Create all the databases, using the array above
    for (let dbName of databases) {
        const db = con.database(dbName);
        if (!await db.existsAsync())
            await db.createAsync();
    }

    const db = con.database('comp90024');
    // Create our database design docs if they don't already exist
    for (let design of design_docs) {
		await db.saveAsync(design['_id'], design);
    }
}

setup();
