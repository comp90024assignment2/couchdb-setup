FROM node:9.11

WORKDIR /home/node/app

ADD . /home/node/app

RUN npm install .

ENTRYPOINT ["node", "index.js"]
