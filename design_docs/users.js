module.exports = {
    "_id": "_design/users",
  "views": {
    "all": {
      "map": "function (doc) {\n                if (doc.type == 'user')\n                    emit(doc._id);\n            }"
    },
    "byLastScraped": {
      "map": "function (doc) {\n  if (doc.type == 'user')\n    emit([doc.last_scraped, Math.random()]);\n}"
    },
    "countFollowedNum": {
      "reduce": "_count",
      "map": "function (doc) {\n  if (doc.type == 'user')\n  emit(doc.teams.length, doc);\n}"
    },
    "byTeam": {
      "map": "function (doc) {\n  if (doc.type == 'user')\n    emit(doc.teams);\n}",
      "reduce": "_count"
    },
    "byUserId": {
      "map": "function (doc) {\n  if (doc.type == 'user')\n    emit(doc.user_id);\n}"
    },
    "byTeamToUser": {
      "map": "function (doc) {\n  if (doc.type == 'user')\n    doc.teams.forEach(function(team) {\n      emit(team, doc.user_id);\n    })\n}"
    }
  }
};
