module.exports = {
   "_id": "_design/tweets",
  "views": {
    "all": {
      "map": "function (doc) {\n                if (doc.type == 'tweet')\n                    emit(doc._id);\n            }",
      "reduce": "_count"
    },
    "allMatchTweets": {
      "map": "function (doc) {\n  if (doc.type == 'tweet' && doc.match) {\n    emit(doc.match_id, doc)\n  }\n}",
      "reduce": "_count"
    },
    "allMatchSentiment": {
      "map": "function (doc) {\n  var YEAR_SECONDS = 31536000;\n  var toYear = function(time) {\n    return (Math.floor(time/YEAR_SECONDS) + 1970)\n  }\n  if (doc.type == 'tweet' && doc.match) {\n    var date = Number(doc.match.split('.')[0]);\n    var season = toYear(date);\n    emit([doc.team, doc.result, season, doc.match], [doc.emoji_sentiment, doc.sentiment]);\n  }\n}",
      "reduce": "function (keys, values, rereduce) {\n  var result = {\n    emoji_count : 0,\n    emoji_sum : 0,\n    emoji_avg : 0,\n    count_pos : 0,\n    count_neg : 0,\n    sentiment_avg : 0\n  };\n  for (i = 0; i < values.length; i++) {\n    if (rereduce) {\n      result.emoji_sum += values[i].emoji_sum;\n      result.emoji_count += values[i].emoji_count;\n      result.count_pos += values[i].count_pos;\n      result.count_neg += values[i].count_neg;\n    } else {\n      if (values[i][0] != 0) {\n        result.emoji_sum += values[i][0];\n        result.emoji_count += 1;\n      }\n      if (values[i][1] == 'pos') {\n        result.count_pos += 1;\n      } else {\n        result.count_neg += 1;\n      }\n    }\n  }\n  result.emoji_avg = result.emoji_sum * 1.0 / result.emoji_count;\n  result.sentiment_avg = 1.0 * (result.count_pos - result.count_neg) / (result.count_pos + result.count_neg);\n  return (result);\n}"
    },
    "emojiScores": {
      "map": "function (doc) {\n  if (doc.emoji_sentiment)\n  emit(doc.emoji_sentiment, doc.text);\n}"
    },
    "tweetMatchSentiment": {
      "map": "function (doc) {\n  var calcScore = function(scores) {\n    return (scores[3].goals * 6 + scores[3].behinds);\n  }\n  var getLoser = function(match) {\n    var winner = match.winner;\n    if (match.teams[0].name == winner) {\n      return match.teams[0].name;\n    } else {\n      return match.teams[1].name;\n    }\n  }\n  var calcMargin = function(match) {\n    var score1 = calcScore(match.teams[0].scores);\n    var score2 = calcScore(match.teams[1].scores);\n    return (Math.abs(score1 - score2));\n  }\n  if (doc.type == 'tweet' && doc.match) {\n    emit([doc.team, doc.match_id, 1, doc.result], {sentiment: doc.sentiment, emoji_sentiment: doc.emoji_sentiment});\n  }\n  if (doc.type == 'match') {\n    var margin = calcMargin(doc);\n    var loser = getLoser(doc);\n    emit([doc.winner, doc._id, 0], margin);\n    emit([loser, doc._id, 0], (-1 * margin));\n  }\n}"
    },
    "allSentiment": {
      "reduce": "function (keys, values, rereduce) {\n  var result = {\n    emoji_count : 0,\n    emoji_sum : 0,\n    emoji_avg : 0,\n    count_pos : 0,\n    count_neg : 0,\n    sentiment_avg : 0\n  };\n  for (i = 0; i < values.length; i++) {\n    if (rereduce) {\n      result.emoji_sum += values[i].emoji_sum;\n      result.emoji_count += values[i].emoji_count;\n      result.count_pos += values[i].count_pos;\n      result.count_neg += values[i].count_neg;\n    } else {\n      if (values[i][0] != 0) {\n        result.emoji_sum += values[i][0];\n        result.emoji_count += 1;\n      }\n      if (values[i][1] == 'pos') {\n        result.count_pos += 1;\n      } else {\n        result.count_neg += 1;\n      }\n    }\n  }\n  result.emoji_avg = result.emoji_sum * 1.0 / result.emoji_count;\n  result.sentiment_avg = 1.0 * (result.count_pos - result.count_neg) / (result.count_pos + result.count_neg);\n  return (result);\n}",
      "map": "function (doc) {\n  var YEAR_SECONDS = 31536000;\n  var toYear = function(time) {\n    return (Math.floor(time/YEAR_SECONDS) + 1970)\n  }\n  if (doc.type == 'tweet' && doc.created_at) {\n    var season = toYear(doc.created_at);\n    emit([doc.team, season], [doc.emoji_sentiment, doc.sentiment]);\n  }\n}"
    }
  }
};
