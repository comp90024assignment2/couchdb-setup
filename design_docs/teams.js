module.exports = {
   "_id": "_design/teams",
  "views": {
    "all": {
      "map": "function (doc) {\n                if (doc.type == 'team')\n                    emit(doc._id);\n            }"
    },
    "byLowestCursor": {
      "map": "function (doc) {\n  if (doc.type == 'team')\n    emit(doc.cursor || 0);\n}"
    },
    "byName": {
      "map": "function (doc) {\n  if (type == 'team')\n    emit(doc.name);\n}"
    }
  }
};
