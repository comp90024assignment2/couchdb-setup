module.exports = {
  "_id": "_design/matches",
  "views": {
    "all": {
      "map": "function (doc) {\n                if (doc.type == 'match')\n                    emit(doc._id);\n            }"
    },
    "allWinnersLosers": {
      "map": "function (doc) {\n  function getLoser(match) {\n      var winner = match.winner;\n      if (match.teams[0].name == winner) {\n          return match.teams[1].name;\n      } else {\n          return match.teams[0].name;\n      }\n  }\n  if (doc.type == 'match') {\n    var date = Number(doc._id.split('.')[0]);\n    var loser = getLoser(doc);\n    emit([doc.winner, doc.date, doc._id], 'winner');\n    emit([loser, doc.date, doc._id], 'loser');\n  }\n}"
    },
    "winLossPercentage": {
      "reduce": "function (keys, values, rereduce) {\n  var result = {\n    wins : 0,\n    losses : 0,\n    win_percentage : 0,\n    loss_percentage : 0\n  };\n  for (i = 0; i < values.length; i++) {\n    if (rereduce) {\n      result.wins += values[i].wins;\n      result.losses += values[i].losses;\n    } else {\n      if (values[i] == 1) {\n        result.wins += 1;\n      } else {\n        result.losses += 1;\n      }\n    }\n  }\n  result.win_percentage = 1.0 * result.wins / (result.wins + result.losses);\n  result.loss_percentage = 1.0 * result.losses / (result.wins + result.losses);\n  return (result)\n}",
      "map": "function (doc) {\n  var YEAR_SECONDS = 31536000;\n  var toYear = function(time) {\n    return (Math.floor(time/YEAR_SECONDS) + 1970)\n  }\n  function getLoser(match) {\n      var winner = match.winner;\n      if (match.teams[0].name == winner) {\n          return match.teams[1].name;\n      } else {\n          return match.teams[0].name;\n      }\n  }\n  if (doc.type == 'match') {\n    var date = Number(doc._id.split('.')[0]);\n    var season = toYear(date);\n    var loser = getLoser(doc);\n    emit([doc.winner, season, doc._id], 1);\n    emit([loser, season, doc._id], 0);\n  }\n}"
    }
  }
};
